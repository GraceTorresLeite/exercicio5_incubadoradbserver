# Incubadora DBServer CI 

## Revisão e Exercícios 

### Exercícios 

**21) Confira se completaste as guias discutidas até o momento:** 

- [x] √ Building a RESTful Web Service  https://spring.io/guides/gs/rest-service/ 

* https://gitlab.com/GraceTorresLeite/incubadora_aula12.git ✔

- [x] √ Consuming a RESTful Web Service https://spring.io/guides/gs/consuming-rest/ 

* https://gitlab.com/GraceTorresLeite/incubadora_aula13.git ✔

**22) Complete as guias a seguir:**

- [x] Building a RESTful Web Service with Spring Boot Actuator https://spring.io/guides/gs/actuator-service/ 
* https://gitlab.com/GraceTorresLeite/incubadora_aula15.git ✔

- [x] Enabling Cross Origin Requests for a RESTful Web Service https://spring.io/guides/gs/rest-service-cors/ 
* https://gitlab.com/GraceTorresLeite/incubadora_aula16.git ✔

**23) Desenvolva um serviço que permite consultar data e hora do servidor.** 

- [x]  https://gitlab.com/GraceTorresLeite/incubadora_aula14.git ✔

**24) Consulte um serviço que permite obter um endereço de correio postal com base em um código de endereçamento postal.**
*  Utilize o serviço: https://viacep.com.br. ✔ https://gitlab.com/GraceTorresLeite/incubadora_aula14.git

**25) [EXTRA] Acrescente autenticação ao serviço de data e hora do exercício 23).**
*  Usuário: scott, senha: tiger. 
